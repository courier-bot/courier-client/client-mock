{{/* Name given to the PostgreSQL app. */}}
{{- define "postgres.name" -}}
{{- printf "%s-postgres" .Release.Name -}}
{{- end -}}

{{/* Provide environment variables to connect to the PostgreSQL service. */}}
{{- define "postgres.environment-variables" -}}
- name: POSTGRES_HOST
  value: {{ template "postgres.name" . }}
- name: POSTGRES_PORT
  value: "5432"
- name: POSTGRES_DB
  valueFrom:
    secretKeyRef:
      name: {{ template "postgres.name" . }}
      key: database
- name: POSTGRES_USER
  valueFrom:
    secretKeyRef:
      name: {{ template "postgres.name" . }}
      key: user
- name: POSTGRES_PASSWORD
  valueFrom:
    secretKeyRef:
      name: {{ template "postgres.name" . }}
      key: password
{{- end -}}
