class CreateMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.string :message_kind
      t.string :target_messageable
      t.string :context
      t.string :external_message_id

      # Will be used during the callback.
      t.text :fields

      t.timestamps
    end
  end
end
