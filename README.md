# Courier Project Test

This project is used to test the [Courier Server](https://gitlab.com/courier_bot/courier_server).

Note: the `courier_project_test` Docker image is currently private.

## Warning

This project is only for Courier Server testing, and is not intended as a client
or UI to deliver messages.

The installation is under heavy construction. This currently assumes a
Kubernetes cluster on Digital Ocean (the persistent volume claim is currently
set for a Digital Ocean Volume), and the Docker image should be located in a
Gitlab container registry (which is private). This will all be fixed in a future
release, including a public container.

## Prerequisites

A working kubernetes cluster and `kubectl` configuration are required, as well
as the [Helm](https://helm.sh/) package manager for kubernetes.

The Courier Server must already be installed, because the test project
installation must be provided with the courier server's API service name.

## Installation

The Helm chart can be found in the `.helm/` folder. The `values.yaml` inside
shows the values that need to be provided in a values file to the installer, and
how to find them.

The Courier Project Test can be installed by executing the following:

    $ helm install --generate-name --values values.yaml ./.helm/courier-project-test

This will install a single replica of the API behind a load balancer on port
3000, and a postgres database on a [DigitalOcean volume](https://www.digitalocean.com/docs/volumes/)

## Post-Installation

After installation, a kubernetes job will setup the database automatically. Once
this is done, all pods should soon be running and ready.

The only thing left is to tell the Courier Server how to reach the test project
when it needs to request contact information. As the Courier Server's admin UI
is not coded yet, the following steps must be taken:

* connect to one of the Courier Server's API pods - `kubectl exec {CS_POD} -ti -- bundle exec rails console`
* find the correct `Client` object and set its `uri` attribute to the correct ip
and port of the test project's load balancer. The test project is currently set
to listen to the `/courier_server/contact_info` route.

Alternately, the value can be changed manually in the database.

## Usage

A POST request can be sent to the test project at the `/messages` route with the
following body structure:

    {
      "message_kind": "order_created",
      "target_messageable": "user_12345",
      "context": null,
      "fields": {
        "contact_kind": "email",
        "to": "to@example.com",
        "subject": "Subject",
        "content": "Email content"
      }
    }

The message kind and contact kind must of course both be valid in the Courier
Server. The `fields` key defines the response to the Courier Server's request
for contact information.

The Courier Project Test cannot handle delivery failures, and will continue
responding with the same values over and over.
