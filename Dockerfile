FROM ruby:2.6.3-stretch

WORKDIR /project/
COPY . /project/

RUN apt-get update -qq && apt-get install -y nodejs postgresql-client

RUN gem install bundler --no-document && bundle install

EXPOSE 3000
