class MessagesController < ApplicationController
  skip_before_action :verify_authenticity_token

  def new
    rnd = rand(10000)

    @target_messageable = "user_#{rnd}"
    @to_contact_value = 'to@example.com'
    @content = "Content #{rnd}"
    @subject = "Subject #{rnd}"
  end

  def create
    courier_server_host = ENV['COURIER_SERVER_HOST'] || 'localhost'
    courier_server_port = ENV['COURIER_SERVER_PORT'] || '3000'
    courier_server = "#{courier_server_host}:#{courier_server_port}"

    server_response = Faraday.post("http://#{courier_server}/messages",
                                   message_params.to_json,
                                   'Content-Type' => 'application/json')

    external_message_id = JSON.parse(server_response.body).symbolize_keys[:id]

    message = Message.new(message_params)
    message.external_message_id = external_message_id
    message.fields = fields_params
    message.save!
  end

  private

  def message_params
    params.permit(:message_kind,
                  :target_messageable,
                  :context)
  end

  def fields_params
    params.require(:fields).permit!.to_hash
  end
end
