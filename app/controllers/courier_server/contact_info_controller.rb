module CourierServer
  class ContactInfoController < ApplicationController
    protect_from_forgery with: :null_session

    def create
      external_message_id = params.require(:message_id)
      message = Message.find_by(external_message_id: external_message_id)

      response_body = message.fields || {}

      render json: response_body
    end
  end
end
