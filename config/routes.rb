Rails.application.routes.draw do
  resources :messages, only: %i[index new create]

  namespace :courier_server do
    resources :contact_info, only: :create
  end
end
